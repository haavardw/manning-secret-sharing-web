package secrets

import (
	"testing"
)

func Test_setSecretReturnsID(t *testing.T) {
	secrets := NewSecretStore()

	id := secrets.SetSecret("hello")
	if id == "" {
		t.Fatal("No secret id returned")
	}
	if id == "hello" {
		t.Fatal("Returned id reveals plaintext")
	}
}

func Test_getSecretReturnsPlainText(t *testing.T) {
	secrets := NewSecretStore()

	// given
	id := secrets.SetSecret("hello")

	// when
	plainText := secrets.GetSecret(id)
	if plainText != "hello" {
		t.Fatalf("Invalid plain text, expected %s got %s", "hello", plainText)
	}
}
