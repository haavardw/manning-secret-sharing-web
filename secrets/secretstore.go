package secrets

import (
	"crypto/md5"
	"encoding/hex"
	"sync"
)

type SecretStore struct {
	secrets map[string]string // md5 -> plaintext
	lock    sync.Mutex
}

func NewSecretStore() *SecretStore {
	return &SecretStore{
		secrets: make(map[string]string),
	}
}

func (s *SecretStore) GetSecret(id string) string {
	s.lock.Lock()
	defer s.lock.Unlock()
	plainText := s.secrets[id]
	delete(s.secrets, id)
	return plainText
}

func (s *SecretStore) SetSecret(plainText string) string {
	sum := md5.Sum([]byte(plainText))
	key := hex.EncodeToString(sum[:])
	s.lock.Lock()
	defer s.lock.Unlock()
	s.secrets[key] = plainText
	return key
}
