package handlers

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func Test_set_secret_with_empty_json_returns_badrequest(t *testing.T) {

	// given
	server := newTestServer()
	writer := httptest.NewRecorder()
	body := strings.NewReader(`{}`)
	request, _ := http.NewRequest("POST", "/", body)
	expected := 400

	// when
	server.ServeHTTP(writer, request)

	// then
	if writer.Code != expected {
		t.Errorf("Unexpected return code %v, expected %v", writer.Code, expected)
	}
}

func Test_put_secret_not_supported(t *testing.T) {

	// given
	server := newTestServer()
	writer := httptest.NewRecorder()
	request, _ := http.NewRequest("PUT", "/", nil)
	expected := 405

	// when
	server.ServeHTTP(writer, request)

	// then
	if writer.Code != expected {
		t.Errorf("Unexpected return code %v, expected %v", writer.Code, expected)
	}
}
