package handlers

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

type mockSecrets struct {
	secrets map[string]string
}

func newMockSecrets() *mockSecrets {
	return &mockSecrets{
		secrets: make(map[string]string),
	}
}

func (s *mockSecrets) SetSecret(plainText string) string {
	id := plainText
	s.secrets[id] = plainText
	return id
}

func (s *mockSecrets) GetSecret(id string) string {
	return s.secrets[id]
}

func newTestServer() *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/healthcheck", NewHealthCheckHandler())
	mux.HandleFunc("/", NewSecretHandler(newMockSecrets()))
	return mux
}

func Test_healthcheck_returns_200OK(t *testing.T) {

	// given
	server := newTestServer()
	writer := httptest.NewRecorder()
	request, _ := http.NewRequest("GET", "/healthcheck", nil)
	expected := 200

	// when
	server.ServeHTTP(writer, request)

	// then
	if writer.Code != expected {
		t.Errorf("Unexpected return code %v, expected %v", writer.Code, expected)
	}
}

func Test_getting_secret_with_empty_id_returns_404NotFound(t *testing.T) {

	// given
	server := newTestServer()
	writer := httptest.NewRecorder()
	request, _ := http.NewRequest("GET", "/", nil)
	expected := 404
	expectedBody := `{"data":""}` + "\n"

	// when
	server.ServeHTTP(writer, request)

	// then
	if writer.Code != expected {
		t.Errorf("Unexpected return code %v, expected %v", writer.Code, expected)
	}

	if writer.Body.String() != expectedBody {
		t.Errorf(
			"Unexpected return body %s, expected %s",
			writer.Body.String(),
			expectedBody,
		)
	}
}

func Test_set_secret_with_no_plaintext_returns_badrequest(t *testing.T) {

	// given
	server := newTestServer()
	writer := httptest.NewRecorder()
	request, _ := http.NewRequest("POST", "/", nil)
	expected := 400

	// when
	server.ServeHTTP(writer, request)

	// then
	if writer.Code != expected {
		t.Errorf("Unexpected return code %v, expected %v", writer.Code, expected)
	}
}
