package handlers

import "net/http"

func healthCheckHandler(writer http.ResponseWriter, request *http.Request) {
}

func NewHealthCheckHandler() http.HandlerFunc {
	return healthCheckHandler
}
