package handlers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type SecretStore interface {
	GetSecret(id string) string
	SetSecret(plaintext string) string
}

var secrets SecretStore

func getSecretHandler(writer http.ResponseWriter, request *http.Request) {
	id := request.URL.Path[1:]
	plainText := secrets.GetSecret(id)
	b, _ := json.Marshal(map[string]string{
		"data": plainText,
	})
	if plainText == "" {
		http.Error(writer, string(b), http.StatusNotFound)
		return
	}
	fmt.Fprint(writer, string(b))
}

func setSecretHandler(writer http.ResponseWriter, request *http.Request) {
	if request.Body == nil {
		http.Error(writer, "", http.StatusBadRequest)
		return
	}

	var data map[string]string
	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		http.Error(writer, "", http.StatusBadRequest)
		return
	}

	err = json.Unmarshal(body, &data)
	if err != nil {
		http.Error(writer, "", http.StatusBadRequest)
		return
	}

	plainText, found := data["plain_text"]
	if !found {
		http.Error(writer, "plain_text not found", http.StatusBadRequest)
		return
	}

	b, _ := json.Marshal(map[string]string{
		"id": secrets.SetSecret(plainText),
	})
	writer.Header().Set("Content-Type", "application/json")
	fmt.Fprint(writer, string(b))
}

func secretHandler(writer http.ResponseWriter, request *http.Request) {
	if request.Method == "GET" {
		getSecretHandler(writer, request)
	} else if request.Method == "POST" {
		setSecretHandler(writer, request)
	} else {
		http.Error(writer, "bad request", http.StatusMethodNotAllowed)
	}
}

func NewSecretHandler(secretStore SecretStore) http.HandlerFunc {
	secrets = secretStore
	return secretHandler
}
