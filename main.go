package main

import (
	"net/http"

	"gitlab.com/haavardw/manning-secret-sharing-web/handlers"
	"gitlab.com/haavardw/manning-secret-sharing-web/secrets"
)

func main() {
	secretStore := secrets.NewSecretStore()
	mux := http.NewServeMux()

	mux.HandleFunc("/healthcheck", handlers.NewHealthCheckHandler())
	mux.HandleFunc("/", handlers.NewSecretHandler(secretStore))
	http.ListenAndServe(":8080", mux)
}
